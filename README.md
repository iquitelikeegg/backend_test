MagicMVC
========

Simple (Magic) MVC Framework for PHP applications.

This is designed to be a straightforward, extensible MVC Framework which enables you to quickly deploy a static webpage with templating features and the ability to support database functionality with no dependencies. It is ideal for beginners to OOP and web development Frameworks - who want to write well structured applications without too much time spent in configuration!

If you're unsure what MVC is - it stands for "Model-View-Controller" and is a model for web application architecture. Read more here! http://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller

Features:

Ready to Go! - Download MagicMVC, set up the WEB_ROOT constant in index.php and you're ready to go!
Optional database functionality will require setting up a few more constants to be set up in the ControllerAbstract (app/lib/ControllerAbstract).
The included .htaccess will rewrite the url so no need to fiddle with apache configurations or htaccess yourself.

Simple! - A simple, logical directory structure and OOP style keeps your code organised and modular.
The majority of code in MagicMVC can be written as regular PHP, JS, HTML and CSS, with no quirks, or custom library beyond the single Page Class - for outputting templated Pages, this keeps the learning curve shallow for beginner developers.

Structured! - The directory structure and coding conventions used in MagicMVC mirror the code structure used in the major PHP Frameworks, making it a great introduction to the world of MVC application development using Frameworks like Zend or Symfony.

Usage:

1. After downloading MagicMVC, make sure the index.php file and "app" and "template" directories are in the application root, set up the WEB_ROOT constant in /index.php for your web includes. For most development purposes this will be "http://localhost/". 

Now if you visit your application in the browser you should see MagicMVC's Hello World page! You're now ready to start adding your content!

2. For the root page, you can modify the existing index "controller" (app/index.php). This combines with the template "index.phtml" (template/index/index.phtml) - which stores the body output for the page.
	b. The header and footer templates are located in template/snippets/... and are empty by default for your own code!

3. To add default scripts modify the Page::create() function in app/lib/model/Page.php to include your own css and script files in every page created using the Page class.

4. To enable Database functionality - modify the database parameters in the ControllerAbstract.php file (app/lib/ControllerAbstract.php)

5. For convention - when adding a new Controller (for a related set of pages) - the controllers are stored in the app/ directory and the template files are stored in the template folder under a directory named after the controller which the template is associated with. Similar conventions should be applied to the js and css directories.

If you have any questions then please drop me a message! 
