<?php
/**
 * Mana_Framework_v1
 *
 * written by Vincent Chan
 *
 * This is a lightweight MVC-like Framework for serving up static content pages
 * with limited requirement for Database functionality.
 *
 * This bootstrapper will accept a $_GET["param"] and generate the necessary routing
 * information from it. This $_GET['param'] is rewritten by the htaccess into a forwardslash separated url structure.
 *
 * The expected url structure for this application is:
 * __PROTOCOL__//__HOST__/controller/action/parameter/value/parameter/value/...
 *
 */

/**
 * use these for include paths
 */
define ('APP_DIR', 'app' . DIRECTORY_SEPARATOR);
define('BASE_APP_DIR', @realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . APP_DIR) . DIRECTORY_SEPARATOR);
define('BASEDIR', @realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR);

/**
 * use this for web includes.
 * ensure .htaccess is correctly set up
 */
define('WEB_ROOT', 'http://backend.dev.test/'); //development

$includePaths = array(
    BASE_APP_DIR,
    BASE_APP_DIR . 'lib' . DIRECTORY_SEPARATOR,
    BASE_APP_DIR . 'lib'. DIRECTORY_SEPARATOR .'model' . DIRECTORY_SEPARATOR,
    get_include_path()
);

set_include_path(implode(PATH_SEPARATOR, $includePaths));

function app_autoload($class) {
    global $includePaths;

    foreach ($includePaths as $path) {
        $file = $path . $class . '.php';

        if (is_readable($file)) {
            require($file);
            return;
        }
    }

    //Maybe redirect 404?
    echo "Requested class $class is missing, Execution stopped";
    exit;
}

spl_autoload_register('app_autoload');

//Routing
$params   = '';
$class    = '';
$method   = '';
$args     = NULL;
$cmd_path = BASE_APP_DIR;
$fullpath = '';
$file     = '';

if (empty($_GET['params'])) $params = 'index';
else                        $params = $_GET['params'];

$params = trim($params, '/\\');
$parts = explode('/', $params);

foreach ($parts as $part) {
    $part      = str_replace('-', '_', $part);
    $fullpath .= $cmd_path . $part;

    if (is_dir($fullpath)) {
        $cmd_path .= $part . '/';
        array_shift($parts);
        continue;
    }

    if (is_file($fullpath . '.php')) {
        $class = $part;
        array_shift($parts);
        break;
    }
}



if (empty($class)) $class = 'index';

$action = array_shift($parts);
$action = str_replace('-', '_', $action);

if (empty($action)) $action = 'index';

$file = $cmd_path . $class . '.php';

$args = $parts;

if (is_readable($file) == false) {
    echo "requested module $class is missing. Execution stopped";
    exit;
}

$class = new $class($args);

//Do this because calling a method "index" means it will get called by default when parsed?
$action = $action . 'Action';

if (is_callable(array($class, $action)) == false) {
    array_unshift($args, $action);
    $action = 'indexAction';
}

$class->$action();
