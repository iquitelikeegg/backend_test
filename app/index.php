<?php
/**
 * This is an example controller which will display a simple "Hello-world" page
 * Overwrite this page for your own index page
 *
 * @author VincentChan
 */
class index extends ControllerAbstract {
    public function indexAction() {
        if (is_null($_SESSION['user'])) {
            header('Location:' . WEB_ROOT . 'login');
            exit;
        }

        Page::create('template/index/index.phtml')
            ->setTitle('backend test app')
            ->addData($user, 'user')
            ->render();
    }

    public function loginAction() {
        $message = '';

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $user = User::login($_POST['username'], $_POST['password']);

            if ($user === false) {
                $message = "<p style='color: red;'>Incorrect username/password</p>";
            } else {
                $_SESSION['user'] = json_encode($user);

                header('Location:' . WEB_ROOT);
                exit();
            }
        }

        Page::create('template/index/login.phtml')
            ->setTitle('Login')
            ->addData($message, 'message')
            ->render();
    }

    public function registerAction() {
        $message = '';

        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $password_repeat = $_POST['password-repeat'];
            $email = $_POST['email'];

            if (!$username || !$password || !$password_repeat || !$email) {
                $message = '<p style="color: red;">fill in all the fields</p>';
            } else if ($password !== $password_repeat) {
                $message = '<p style="color: red;">passwords do not match</p>';
            } else {
                $user = new User();

                $user->setEmail($email);
                $user->setUsername($username);
                $user->setPassword($password);
                $user->save();

                header('Location:' . WEB_ROOT . 'login');
                exit();
            }
        }

        $page = Page::create('template/index/register.phtml')
            ->setTitle('Register')
            ->addData($message, 'message')
            ->render();
    }
}
