<?php
/**
 * Provides controllers that inherit it with a basic contructor for fetching parameters from the url and a method for instatiating the PDO.
 *
 * @author VincentChan
 */
abstract class ControllerAbstract {

    private $dbHost = 'localhost'; //Your DB host
    private $dbPort = '3306';      //Your DB Port
    private $dbName = 'backend_test'; //Your DB Name
    private $dbUser = 'secondary'; //Your DB User
    private $dbPass = 'aHVycmljYW5lcw=='; //Your DB Pass

    protected $args;

    public function __construct($args) {
        $params = array();
        $isKey  = 0;

        foreach ($args as $key => $arg) {
            // .../param/value/param/value/...
            if ($key === $isKey + 1) {
                continue;
            }

            $isKey = $key;

            $params[$arg] = $args[$key + 1];
        }

        $this->args = $params;
    }

    protected function getPdo() {
        $dsn = sprintf('mysql:host=%s;port=%s;dbname=%s',
            $this->dbHost,
            $this->dbPort,
            $this->dbName
        );

        return new PDO(
            $dsn,
            $this->dbUser,
            $this->dbPass
        );
    }
}
