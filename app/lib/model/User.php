<?php

class User extends ModelBase {

    private $pdo;

    protected $id;
    protected $email;
    protected $username;
    protected $password;
    protected $createdDate;
    protected $userLevel = self::USER_LEVEL_USER;

    const USER_LEVEL_ADMIN = 1;
    const USER_LEVEL_USER  = 0;

    public static function login($username, $password) {
        $user = new User();

        $user->initPdo();

        $statement = $user->pdo->prepare("SELECT id FROM users
            WHERE username = ? AND password = ?");

        $statement->execute(array(
            $username,
            md5($password)
        ));

        $result = $statement->fetchAll();

        if (empty($result)) {
            return false;
        }

        return self::create($result[0]['id'], $user);
    }

    public static function create($id, User $user) {
        $user->initPdo();

        $statement = $user->pdo->prepare("SELECT * FROM users WHERE id = ?");

        $statement->execute(array($id));
        $userData = $statement->fetchAll();

        $user->id    = $id;
        $user->email = $userData['email'];
        $user->username = $userData['username'];
        $user->createdDate = $userData['created_date'];
        $user->userLevel = $userData['user_level'];

        return $user;
    }

    public function save() {
        $this->initPdo();

        if (is_null($this->id)) {
            $statement = $this->pdo->prepare("INSERT INTO
                users ('email', 'username', 'password', 'created_date', 'user_level')
                VALUES (?, ?, ?, NOW(), ?)");

            $statement->execute(array(
                $this->email,
                $this->username,
                $this->password,
                $this->userLevel
            ));
        } else {
            $statement = $this->pdo->prepare("UPDATE users
                SET
                    email = ?,
                    username = ?,
                    password = ?,
                    user_level = ?
                WHERE id = ?");

            $statement->execute(array(
                $this->email,
                $this->username,
                $this->password,
                $this->userLevel,
                $this->id
            ));
        }
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function setPassword($password) {
        $this->password = md5($password);
    }

    public function setCreatedDate($createdDate) {
        $this->createdDate = $createdDate;
    }

    public function setUserLevel($level) {
        $this->userLevel = $level;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getUserLevel() {
        return $this->userLevel;
    }

    private function initPdo() {
        if (is_null($this->pdo)) {
            $this->pdo = $this->getPdo();
        }
    }

    public function __contruct() {}
}
