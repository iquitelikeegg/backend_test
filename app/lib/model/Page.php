<?php

/**
 * a website page. for general use, the Page::create() method will create a
 * page object with default styles and scripts.
 *
 * @author VincentChan
 */
class Page {

    protected $title = 'Mana_Framework_v1';
    protected $scripts;
    protected $styles;
    protected $template;
    protected $data = array();

    /**
     * create a page with the default scripts and stylesheets
     *
     * @param string $template
     * @return \Page
     */
    public static function create($template) {
        $page = new Page($template);

        //define your default scripts here
        $page->addScript('js/external/jquery-1.8.2.js');

        $page->addStyleSheet('css/main.css');

        return $page;
    }

    public function __construct($template) {
        $this->template = $template;
    }

    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    public function addScript($script) {
        $this->scripts[] = $script;
        return $this;
    }

    public function addStyleSheet($styleSheet) {
        $this->styles[] = $styleSheet;
        return $this;
    }

    /**
    * Pass parameters to the template, the variable name as seen in the template will be the $key string
    *
    * @param mixed $data The variable data
    * @param string $key The variable name, conforms to all normal PHP variable naming rules
    * @return \Page
    */
    public function addData($data, $key) {
        $this->data[$key] = $data;
        return $this;
    }

    /**
     * render the page
     */
    public function render() {
        foreach ($this->data as $var => $data) {
            ${$var} = $data;
        }

        $head = "<!DOCTYPE html>
            <html>
                <head>
                    <title>$this->title</title>
                    <meta charset='utf-8'>
                    <link rel='icon' type='image/png' href='" . WEB_ROOT . "favicon.png'>";

        foreach ($this->styles as $styleSheet) {
            $head .= '<link rel="stylesheet" type="text/css" href="' . WEB_ROOT . $styleSheet . '" />';
        }

        foreach ($this->scripts as $scriptPath) {
            $head .= '<script type="text/javascript" src="' . WEB_ROOT . $scriptPath . '"></script>';
        }

        $head .= '</head>';

        ob_start();

        echo $head;
        echo '<body>'
        . '     <div class="background">';

        include_once($this->template);

        echo '</div>
                </body>
            </html>';

        $output = ob_get_clean();

        echo $output;
    }
}
