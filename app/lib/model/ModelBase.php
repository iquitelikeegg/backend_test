<?php

abstract class ModelBase {

    private $dbHost = 'localhost'; //Your DB host
    private $dbPort = '3306';      //Your DB Port
    private $dbName = 'backend_test'; //Your DB Name
    private $dbUser = 'secondary'; //Your DB User
    private $dbPass = 'aHVycmljYW5lcw=='; //Your DB Pass

    protected function getPdo() {
        $dsn = sprintf('mysql:host=%s;port=%s;dbname=%s',
            $this->dbHost,
            $this->dbPort,
            $this->dbName
        );

        return new PDO(
            $dsn,
            $this->dbUser,
            $this->dbPass
        );
    }
}
